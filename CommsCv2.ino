#include <Servo.h>

//Roller golabal variables
Servo clampServo; //servo that moves the clamp
Servo beltMotor; //motor that moves the belt

int clampPin = 12; //placeholder pin numbers
int beltMotorPin = 11;
int clampOnPin = 4;
int beltOnPin = 5;

int motorSpeedForward = 180; //max, min and stop values in microseconds
int motorSpeedReverse = 0;
int motorStop = 90;
int clampRest = 50; //clamp rest and engaged values in microseconds
int clampEngage = 90;

//Arm movement and can detection
Servo rollerServo;
int armPin = 6; 
int canPin = 10;
int armPos1 = 0; // arm position should 0 to 180 degree in angle

//L-shape Global Variables
//Motor
Servo lServo;
int lMotorSpeedForward = 0; 
int lMotorSpeedReverse = 180;
int lMotorStop = 90; 
//Button
int buttonState = 0;
int button = 40;
//L-shape Sensor
const float VCC = 4.98; // Measured voltage of Ardunio 5V line
const float R_DIV = 10000.0; // Measured resistance of 3.3k resistor
const float STRAIGHT_RESISTANCE = 27200.0; // resistance when straight
const float BEND_RESISTANCE = 60000.0; //
const int sensor = A3;
float threshold = 50;
int sensorState= 0;// variable to store the value read from the sensor pin
//L-shape Switch
int Switch = 41;
int SwitchState = 0;
int previousSwitchState = 0;

//Outtake global variables
int solenoidpin = 2;    //defines solenoid @pin 4 <-- Change as required
int timer = (1000) + 5000; // Gets can count and opens the lock until for timer + 5 secs 

int robotPosition = 1; //1 is ready to collect cans, 0 is not (set to 1 for testing purposes)
int armIdentifier = 28;
bool armState = NULL; //true -> l-shape, false -> roller

void setup() {
  pinMode(armIdentifier, INPUT); //declare belt motor pin to check pulse time
  //if no pulse is dectected, we use the l-shape design
  if(digitalRead(armIdentifier) == LOW){ //CHANGE TO PIN FOR ARM
    //L-shape setup
    pinMode(button,INPUT);
    lServo.attach(6,1000,2000);
    pinMode(sensor,INPUT);
    pinMode(Switch,INPUT_PULLUP);
    armState == false;
  }
  else if(digitalRead(armIdentifier) == HIGH){ //else, use the roller setup
    //Roller setup
    //Note: The speeds and positions of motors and clamp can be changed from a 0 - 180 range rather
    //      than a 1000 - 2000 range by changing writeMicroseconds to just write and changing the motor
    //      speed, stop and clamp rest values accordingly.
    pinMode(beltOnPin, OUTPUT);
    pinMode(clampOnPin, OUTPUT); //initialises output pins
    clampServo.attach(clampPin, 1000, 2000); //designates signal pin for clamp and its min and max speeds
    clampServo.writeMicroseconds(clampRest); //defaults clamp to resting position
    beltMotor.attach(beltMotorPin, 1000, 2000);   //designates signal pin for the belt motor
    beltMotor.writeMicroseconds(motorStop);       //defaults the motor to stop
    
    pinMode(armPin,OUTPUT); 
    pinMode(canPin,INPUT);
    rollerServo.attach(armPin,1000,2000);

    armState == true;
  }
  //Outtake setup
  pinMode(solenoidpin, OUTPUT); //sets solenoid as Output

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(robotPosition == 1){
    getCans(10);//*example* cans to collect will be given to us
  }
}

void armExtend (){
  // assuming robots in position
  // set arm extend speec
  //
  rollerServo.write(motorSpeedForward);
}

void armRetract(){
  rollerServo.write(motorSpeedReverse);
}

void armStop(){
  rollerServo.write(motorStop);
}

boolean canDetect(){ //Roller 
  if(digitalRead(canPin) == HIGH){ // Digital read gives the pulse and the can is counted. Proximity sensor
    return true;
  }
  else{
    return false;
  }
}

//Pre: The robot has positioned itself with a can in range of the belt and clamp
//Post: The clamp will clamp onto the can and hold it in place
void clamp(){
  clampServo.writeMicroseconds(clampEngage);
}

//Pre: The clamp is in the clamped position and the belt motor is off
//Post: The clamp will move back to its unclamped state
void unclamp(){
  clampServo.writeMicroseconds(clampRest);
}

//Pre: The clamp is engaged and has a can in it's grasp
//Post: The belt will run for a period of time then switch off
void beltRun(){
  unsigned long beltTimer = millis(); //timer for how long the belt has been running for and time spent
  const long beltOffTime = 10000;     //running before it shuts off in miliseconds (10 seconds)
  if(clampServo.read() == 50){
    beltMotor.write(motorSpeedForward); //could also be backwards depending on configuration
    if(beltTimer >= 10000){
      beltMotor.write(motorStop);
    }
  }
}

int lock(){
  digitalWrite(solenoidpin, LOW);  //sets the solenoid into HIGH state
  return 1; //confirms that door has been locked
}

void unlock(){
  digitalWrite(solenoidpin, HIGH);  //sets the solenoid into HIGH state
  delay(timer);
  lock(); //auto-locks once time is up.
}

int lShape(int num){
  int numberOfCans = 0;
  
  buttonState = digitalRead(button);
  SwitchState = digitalRead(Switch);
  
  lServo.write(lMotorSpeedForward); //turns the motor On
  delay(500); // waits for onTime milliseconds
  
  int flexSensor = analogRead(sensor);
  float flexV = flexSensor * VCC / 1023.0;
  float flexR = R_DIV * (VCC / flexV - 1.0);
  // Use the calculated resistance to estimate the sensor's
  // bend angle:
  float angle = map(flexR, STRAIGHT_RESISTANCE, BEND_RESISTANCE,
                   0, 90.0);
  delay(500);
  if(angle >= threshold){ //sensors get pressed 
    numberOfCans++; //count++
  }
 
  if(buttonState || numberOfCans == num){ //buttonPressed or get enough cans
    lServo.write(lMotorStop); //motorStop
    delay(500);

    // turns the motor pin to another direction
    lServo.write(lMotorSpeedReverse);
    if (SwitchState != previousSwitchState) {
    // if the state has changed,stop motor
      if (SwitchState == HIGH) {
        lServo.write(lMotorStop);
        // waits for offTime milliseconds
        delay(500);
      }
    }
  return numberOfCans;
  }
}

int getCans(int numOfCans) { //Roller
  int count = 0;
  if(armState == false) {
    while(count < numOfCans){ 
      armExtend();
      while(canDetect() == false){
        armExtend();
        if (canDetect() == true) {
          armStop();
          break;
        }
      }
      if(canDetect() == true){
        armStop();
        clamp();
        if(clampServo.read() == clampRest){  //guarding beltRun function so it only runs if clamp is in engaged position
          beltRun();
        }
        unclamp();
        count++;
      }
    }
  return count;
  }
  else if(armState == true) {
    return lShape(numOfCans);
  }
}

void unload(){
  unlock();
  lock();
}
